<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('amount')->nullable();
            $table->string('buyer')->nullable();
            $table->string('buyer_name')->nullable();
            $table->string('buyer_phone')->nullable();
            $table->string('currency')->nullable();
            $table->string('fees')->nullable();
            $table->string('longurl')->nullable();
            $table->string('mac')->nullable();
            $table->string('payment_id')->nullable();
            $table->string('payment_request_id')->nullable();
            $table->string('purpose')->nullable();
            $table->string('shorturl')->nullable();
            $table->string('status')->nullable();
            $table->text('all')->nullable();/*
            $table->integer('customers_id')->unsigned();
            $table->foreign('customers_id')->references('id')->on('customers');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_requests');
    }
}
