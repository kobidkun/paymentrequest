<?php

namespace App\Http\Controllers\publiccontroller;

use App\Customer;
use App\PaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Softon\Indipay\Facades\Indipay;

use Yajra\DataTables\DataTables;


class CustomerPaynow extends Controller
{



    public function __construct()
    {
        $this->middleware('auth');
    }

    public function GetPaymentForm(){
        return view('admin.page.createpayment');
    }
    public function PostPaymentForm(Request $request){
        $name = $request->fname;
        $s = new PaymentRequest();

        $s->amount = $request->amount;
        $s->buyer = $request->email;
        $s->buyer_name = $name ;
        $s->buyer_phone = $request->mobile;
        $s->currency = 'INR';
        $s->fees = $request->amount;
        $s->status = 'pending';
        $s->purpose = 'Package Booking';
        $s->slug = md5(microtime());
        $s->save();

        $lastid = $s->id;

        $fpr = PaymentRequest::findorfail($lastid);
        //dd($fpr);

        /*send sms to user*/





        //Your authentication key
        $authKey = "143565AZab5cu0T5a8b1034";

//Multiple mobiles numbers separated by comma
        $mobileNumber = $request->mobile;

//Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "PAYSUC";

//Your message to send, Add URL encoding here.
        $message = urlencode("Hi ". $request->fname.'Please pay Rs '.$request->amount.' by clicking here:'. ' http://northeastlive.website/requested-payments/'.$s->slug);

//Define route
        $route = "4";
//Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

//API URL
        $url="http://api.msg91.com/api/sendhttp.php";

// init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


//Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


//get response
        $output = curl_exec($ch);

//Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);












        /*send sms to user end*/

        return view('admin.page.viewpaymentlink',['details' =>  $fpr]);
    }

    public function GetPaymentFormsuccess(){
        return view('admin.page.viewpaymentlink');
    }

    public function viewAllPayments(){
        return view('admin.page.viewpayment');
    }


    public function viewAllPaymentsdatatableapi()
    {
        $customers = PaymentRequest::select(
            [
                'id',
                'buyer_name',
                'buyer_phone',
                'amount',
                'status',
                'created_at'
            ]);

        return Datatables::of($customers)
            ->addColumn('action', function ($user) {
                return '<a target="_blank" href="/paynow/status/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i>
 View Details</a>';
            })
            ->editColumn('amount', 'Rs: {{$amount}}')
            ->removeColumn('password')
            ->make(true);
    }


    public function viewpaymentdetails(Request $request,$id){
       $f = PaymentRequest::findorfail($id);
        return view('admin.page.viewpaymentlink',['details' => $f]);

    }

















}

