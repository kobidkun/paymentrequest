<?php

namespace App\Http\Controllers\publiccontroller;

use App\PaymentRequest;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Softon\Indipay\Facades\Indipay;
class CustomerPublic extends Controller
{
    public function ShowPaymenttoUser(Request $request, $slug){

        $find = PaymentRequest::where('id', $slug)
            ->orWhere('slug', $slug)
            ->firstOrFail();
//dd($find);

        return view('admin.publicpages.viewform')->with(['data' => $find]);
    }

    public function ShowPaymenttoUsersubmit(Request $request){

        $parameters = [

            'purpose' => 'package booking',

            'buyer_name' => $request->buyer_name,
            'email' => $request->email,
            'phone' => $request->mobile,



            'amount' => $request->amount,

        ];

        $order = Indipay::prepare($parameters);

        //return response()->json($order);
        // dd($order);

        $c = PaymentRequest::findorfail($request->id);

        $c->payment_id = $order->response->payment_request->id;
        $c->save();



        //dd($order);
        return Indipay::process($order);

        // return view('admin.publicpages.viewform')->with(['data' => $find]);
    }




    public function ReceievePaymentStatus(Request $request){
        // For default Gateway
        $response = Indipay::response($request);

        // For Otherthan Default Gateway
        //  $response = Indipay::gateway('NameOfGatewayUsedDuringRequest')->response($request);
//return response()->json($response);
        // dd($response);
        //  dd($response->payment_request);

        if ($response->success = 'true') {

            $a =    PaymentRequest::where('payment_id', $response->payment_request->id)->firstOrFail();

            $a->status = 'success';
            $a->payment_request_id = $response->payment_request->payment->payment_id;
            $a->all = \GuzzleHttp\json_encode($response);


            $a->save();


            return view('admin.publicpages.paymentstatus', ['data' => $a,'status'=>'Success']);
        }
        else{

            echo 'your payment failed';

        }

    }
}
