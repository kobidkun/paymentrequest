<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentRequest extends Model
{
    public function users(){
        return $this->hasOne('App/Customer','id','customer_id');
    }
}
