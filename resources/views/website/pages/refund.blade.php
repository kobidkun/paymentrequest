@extends('website.index');
@section('content')

    <main>




        <div class="container margin_60">

<p>


    The advance payment 50% of package rate is a confirmation amount & it is non refundable on cancellation of booking.

    Cancellation on tourist request paid amount is non refundable at any circumstance.

    If cancellation is done by tourist before 15 day of trip, in that case tourist has to pay total 70% of booking bill amount.

    In case tourists cancel any booking before 7 days of trip or same day cancellation, in that case 100% of bill amount is chargeable to tourists.

    After an advance payment if tourist want to customize the programme is not permitted, If tourist wants to customize in that case the advance will be non refundable & first quote will get cancel & will have to proceed with another booking with a fresh programme quotation ,here first quotation advance amount will not be refunded or adjusted at any circumstance. All extra payment has to pay by tourist on spot for spot cancellation of any program.

    Customization of trip is not possible while travelling in trip, if so then tourist must pay total billing amount (100%) & here after all extra must bear by tourists themselves. Customization if possible can be done by management otherwise all risk & responsibilities go to tourists, travel management will quit from it & will not be responsible at any circumstance.

    N.B. - An advance payment for booking is considering that tourist are satisfied & gone through the quote & there will be no refund, reduction in the cost for any un-utilized services forming part of the tour. Company holds the rights to change alter or amend the itinerary and hotels without prior notice.

    Jurisdictions: Subject to Siliguri Jurisdiction only.


</p>



        </div><!-- End container -->
    </main><!-- End main -->

@endsection