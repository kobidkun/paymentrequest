@extends('website.index');
@section('content')

    <main>
        <!-- Slider -->
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                        <!-- MAIN IMAGE -->
                        <img src="website/tourimg/1.jpg" alt="slidebg1" data-lazyload="website/tourimg/1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption white_heavy_40 customin customout text-center text-uppercase"
                             data-x="center" data-y="center" data-hoffset="0" data-voffset="-20"
                             data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">SWEET TRIP OF BHUTAN
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 text-center" data-x="center" data-y="center"
                             data-hoffset="0" data-voffset="15"
                             data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                             data-speed="500" data-start="2600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05"
                             data-endelementdelay="0.1" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                            <div style="color:#ffffff; font-size:16px; text-transform:uppercase">
                                SWEET TRIP OF BHUTAN</div>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-y="center" data-hoffset="0" data-voffset="70" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2900" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12;"><a href='all_tour_list.html' class="button_intro">View</a> <a href='single_tour.html' class=" button_intro outline">Read more</a>
                        </div>
                    </li>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                        <!-- MAIN IMAGE -->
                        <img src="website/tourimg/2.jpg" alt="slidebg1" data-lazyload="website/tourimg/2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption white_heavy_40 customin customout text-center text-uppercase" data-x="center" data-y="center" data-hoffset="0" data-voffset="-20" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">More than 100 tours available
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 text-center" data-x="center" data-y="center" data-hoffset="0" data-voffset="15" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                            <div style="color:#ffffff; font-size:16px; text-transform:uppercase">
                                BEST OF BHUTAN</div>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-y="center" data-hoffset="0" data-voffset="70" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2900" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12;"><a href='all_tour_list.html' class="button_intro">View tours</a> <a href='single_tour.html' class=" button_intro outline">Read more</a>
                        </div>
                    </li>

                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                        <!-- MAIN IMAGE -->
                        <img src="website/tourimg/3.jpg" alt="slidebg1" data-lazyload="website/tourimg/3.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption white_heavy_40 customin customout text-center text-uppercase" data-x="center" data-y="center" data-hoffset="0" data-voffset="-20" data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">Discover fantastic places
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 text-center" data-x="center" data-y="center" data-hoffset="0" data-voffset="15" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                            <div style="color:#ffffff; font-size:16px; text-transform:uppercase">
                                THE ROYAL BHUTAN</div>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-y="center" data-hoffset="0" data-voffset="70" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2900" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12;"><a href='all_tour_list.html' class="button_intro">View tours</a> <a href='single_tour.html' class=" button_intro outline">Read more</a>
                        </div>
                    </li>

                    <!-- SLIDE  -->



                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
        <!-- End Slider -->
        <div class="container margin_60">

            <div class="main_title">
                <h2>Bhutan <span>Top</span> Tours</h2>
                <p>Top Tour Packages to Bhutan</p>
            </div>

            <div class="row">

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="/tour-1.php">
                                <img src="website/packages/1.jpg" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>THE ROYAL BHUTAN</strong>Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i>
                                <i class="icon-smile voted"></i>  <i class="icon-smile voted"></i><small>(75)</small>
                            </div><!-- end rating -->

                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.2s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="#">
                                <img src="website/packages/2.jpeg" width="800" height="533" class="img-responsive" alt="Image">

                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>BEST OF BHUTAN</strong> Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div><!-- end rating -->
                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.3s">
                    <div class="tour_container">
                        <div class="ribbon_3 popular"><span>Popular</span></div>
                        <div class="img_container">
                            <a href="#">
                                <img src="website/packages/3.jpeg" width="800" height="533" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>MAJESTIC BHUTAN</strong> Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div><!-- end rating -->

                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.4s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="#">
                                <img src="website/packages/4.jpeg" width="800" height="533" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>A COMPLETE BHUTAN</strong>Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div><!-- end rating -->

                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.5s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="#">
                                <img src="website/packages/5.jpeg" width="800" height="533" class="img-responsive" alt="Image">

                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>HIMALAYAN DAUGHTER</strong> Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div><!-- end rating -->

                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->

                <div class="col-md-4 col-sm-6 wow zoomIn" data-wow-delay="0.6s">
                    <div class="tour_container">
                        <div class="ribbon_3"><span>Top rated</span></div>
                        <div class="img_container">
                            <a href="#">
                                <img src="website/packages/6.jpg" width="800" height="533" class="img-responsive" alt="Image">

                            </a>
                        </div>
                        <div class="tour_title">
                            <h3><strong>SWEET TRIP OF BHUTAN</strong> Tour</h3>
                            <div class="rating">
                                <i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile voted"></i><i class="icon-smile"></i><small>(75)</small>
                            </div><!-- end rating -->
                        </div>
                    </div><!-- End box tour -->
                </div><!-- End col-md-4 -->


            </div><!-- End row -->

        </div><!-- End container -->



        <div class="container margin_60">

            <div class="main_title">
                <h2>Some <span>good</span> reasons</h2>
                <p>
                    Why Should you choose Us?
                </p>
            </div>

            <div class="row">

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>+120</span> Premium tours</h3>
                        <p>
                            Best Tour And Travel Provider for Buttan
                        </p>
                        <a href="/" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>+1000</span> Customers</h3>
                        <p>
                            We have more then 7500+ Smiling Customers
                        </p>
                        <a href="/" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-md-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>H24 </span> Support</h3>
                        <p>
                            Get 24x7 Dedicated Support from us
                        </p>
                        <a href="/" class="btn_1 outline">Read more</a>
                    </div>
                </div>

            </div><!--End row -->



        </div><!-- End container -->
    </main><!-- End main -->

    @endsection