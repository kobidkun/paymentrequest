@extends('website.index');
@section('content')

    <main>




        <div class="container margin_60">

<p>


    An agreement of deal will be confirmed once an advance as 50% of total billing amount will be received by us. On receipt you will get an advance voucher via mail.

    All booking confirmation will be sent by sales department via mail after your payment received by us. Hotels are subject to get rooms avail.

    All quotations will be sent with Expected Tour Itinerary on your requirement basic & confirm Itinerary will be provided via mail by sales team once booking formalities will be completed by Tourist. In confirmation mail tourist will received all details as necessary.

    Tourist must carry their individual ID proof (i.e. - Voter ID/ AADHAR Card/ Pass Port/Driving licence or any Govt.ID as valid) while travelling during trip. Packages are valid for only Indian citizen, in case of foreign tourists they must inform us before booking or they must have inner line permit or valid visa.

    Tourist must submits their documents copy with Photos to our concern person while to apply any kind of Permit.

    Nathula Pass of Sikkim is a subject of Sikkim Tourism Department ,we will be abale to show our guest if permitted then only,Travel Managemnet Office is not Resposible if Nathula Pass is not issued.Also in case for any reson Tsomgo Lake & Baba Mandir is closed or Permit is not issued our travels office is not responsible.

    During trip tourist must take care their all valuable goods & luggage at own risk, missing of any item of tourist travel management is not responsible.

    All the vehicles will be arranged by the management for tourist as per the itinerary, they can’t use the vehicle for self driving or to their personal use. Tourist must understand the RTO rules of hill area. Vehicles will be arranged for Point to Point service basis as per the programme set. For any kind of technical fail of vehicles or accident Travel management will not be responsible, if possible as per the situation car can be replaced otherwise it will be considered as an unfortunate incident as it is not an human error.

    Trips are excluding insurance, if Traveller/Tourist needs to have travelling insurance so they can arranged by them or we can provide assistance on extra chargeable basis as on actual cost depends on concern insurance company. Travel management is also not responsible for any claims of insurance, if any claim able incident occurred.

    During the trip Travel management is not responsible if any incident happened as act of GOD. Travel office is also not responsible during programme for any obstruction occurred due to local violence, election or strike. Act of God means earthquake, flood, and storms, heavy down falls, as like all type incidents as happen paranormally or supernaturally. In this case we need a good co ordination from tourists & they must understand situations.

    All meal as included in the package will be served in specific hotels or resorts during the stay at own time of hotels as normally served in hospitality industry. There are total four type plans in Hospitality industry as follows in the packages,-

    EUROPEAN PLAN:  It  means only stay no meals included, CONTINENTALPLAN: stay includes with only breakfast, MODIFIED AMERICAN PLAN: Stay with Breakfast, lunch or Dinner & one more plan called AMERICAN PLAN: stay with Breakfast, Lunch & Dinner.

    Any extra expense during the programme if done by tourist must bear by him or her own, Travels management is not libel to pay any extra as out of package as per quotation mail. Any extra meals during stay even any beverage order must be paid by tourist before check out form hotels or resorts. On the way meals is not included in packages until not mention in mail or quotation.

    Travel Management has right to cancel any agreement after confirmation of programme if anything found wrong or fake. This will be done by sending a mail to the concern party or to the booker.

    Shearing transportation is a subject to get availability of vehicle, if any programme confirmation happened on the base of shearing vehicle we expect from tourist coordination, consider this truth & in case of non availability of shearing Vehicle it may cause the delay of trip, so for the same tourist must hire a reserve car for better service on extra pay on actual.

    Tourist must follow all rules of Local Tourism during trip, (Example, - SIKKIM & BHUTAN smoking restricted area) it may cause of penalty.



</p>



        </div><!-- End container -->
    </main><!-- End main -->

@endsection