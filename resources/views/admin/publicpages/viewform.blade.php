

           @extends('admin.public')


           @section('con')

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal"
                                  action="{{route('public.payment.form.submit')}}"
                                  method="post"
                                  role="form">
                                <div class="form-body">
                                    @if($data->status === 'pending')

                                        <h3 class="box-title">
                                            Payment Requested Details
                                        </h3>

                                    @else
                                        <h3 class="box-title">
                                            You have already paid hence you cannot pay again

                                        </h3>

                                    @endif




                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3 text-left">Name:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$data->buyer_name}} </p>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Payment id:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$data->payment_id}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Amount:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$data->amount}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Email:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$data->buyer}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Mobile:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$data->buyer_phone}} </p>


                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <!--/span-->
                                    </div>
                                    <!--/row-->

                                    <input type="hidden" name="id" value="{{$data->id}}">
                                    <input type="hidden" name="buyer_name" value="{{$data->buyer_name}}">
                                    <input type="hidden" name="amount" value="{{$data->amount}}">
                                    <input type="hidden" name="email" value="{{$data->buyer}}">
                                    <input type="hidden" name="mobile" value="{{$data->buyer_phone}}">
                                    <input type="hidden" name="payment_id" value="{{$data->payment_id}}">
                                    <input type="hidden" name="tid" value="{{$data->slug}}">

                                    @if($data->status === 'pending')

                                        <button class="btn btn-primary btn-block" type="submit">Pay Now</button>

                                        @else
                                        <a href="#"
                                                class="btn btn-success btn-block" >
                                            You have already paid hence you cannot pay again

                                        </a>

                                        @endif


                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>








               @endsection

