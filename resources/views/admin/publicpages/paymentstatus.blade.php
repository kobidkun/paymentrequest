@extends('admin.public')

@section('con')


<div class="page-wrapper">

    <div class="container-fluid">


        {{--main form--}}
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white"> {{$status}}</h4>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal"
                              method="post"
                              role="form">
                            <div class="form-body">
                                @if($data->status === 'pending')

                                    <h3 class="box-title">
                                        Payment Requested Details
                                    </h3>

                                @else
                                    <h3 class="box-title">
                                        You have Successfully paid
                                    </h3>

                                @endif
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Name:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$data->buyer_name}} </p>
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Payment id:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$data->payment_id}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Amount:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$data->amount}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Email:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$data->buyer}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="control-label text-right col-md-3">Mobile:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$data->buyer_phone}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--/span-->
                                </div>
                                <!--/row-->
                                    @if($data->status === 'pending')



                                    @else
                                        <a href="#"
                                           class="btn btn-success btn-block" >
                                            You have Successfully paid

                                        </a>

                                    @endif
                                {{--ids--}}


                                {{--ids--}}

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- .row -->

















        {{--main form end--}}

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer">
        © 2017 Admin Press Admin by themedesigner.in
    </footer>

</div>

    @endsection