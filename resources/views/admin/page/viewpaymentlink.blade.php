@extends('admin.index')

@section('content')



    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Payment Request for {{$details->buyer_name}} </h3>
            </div>


        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->





            {{--main form--}}




            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Payment Requested <strong>{{$details->status}}</strong></h4>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal"
                                  role="form">
                                <div class="form-body">
                                    <h3 class="box-title">Payment Requested Details </h3>
                                    <hr class="m-t-0 m-b-40">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Name:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$details->buyer_name}} </p>
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Payment id:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$details->payment_id}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Amount:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$details->amount}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Email:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$details->buyer}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Mobile:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$details->buyer_phone}} </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Link:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">

                                                        <a href="https://www.northeastlive.website/requested-payments/{{$details->slug}}">
                                                            https://www.northeastlive.website/requested-payments/{{$details->slug}}

                                                        </a>

                                                         </p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-3">Status:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static">

                                                        {{$details->status}}

                                                         </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->

                                        <!--/span-->
                                    </div>
                                    <!--/row-->

                                    {{--ids--}}


                                    {{--ids--}}

                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-offset-12 col-md-12">


                                                    <a
                                                            target="_blank"
                                                            class="btn btn-info btn-block"
                                                            href="whatsapp://send?text=Hi {{$details->buyer_name}} to pay Amount of Rs {{$details->amount}} please visit  https://www.northeastlive.website/requested-payments/{{$details->slug}}"

                                                            data-action="share/whatsapp/share"

                                                    ><i class="fa fa-whatsapp"></i>Share via Whatsapp


                                                    </a>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .row -->



            <a class="btn btn-block waves-effect waves-light btn-warning" href="{{route('admin.form.viewall.payment')}}">All Payments </a>
            <a class="btn btn-block waves-effect waves-light btn-success" href="{{route('admin.form.payment.get')}}">Create Payments </a>














            {{--main form end--}}

        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2017
        </footer>

    </div>







@endsection


@section('footer_script')


@endsection