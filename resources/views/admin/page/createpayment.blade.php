@extends('admin.index')

@section('content')




<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Request Payment</h3>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->





        {{--main form--}}




        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Request Payment</h4>
                        <h6 class="card-subtitle">Serach for user if exists</h6>


                        <form class="form-material m-t-40"
                        action="{{route('admin.form.payment.post')}}"
                              method="post"
                        >

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="fname" required
                                       class="form-control form-control-line">
                            </div>



                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" required
                                       class="form-control form-control-line">
                            </div>

                            <div class="form-group">
                                <label>Mobile</label>
                                <input type="text" name="mobile" required
                                       class="form-control form-control-line">
                            </div>

                            <div class="form-group">
                                <label>Amount in Rs.</label>
                                <input type="number" name="amount" required
                                       class="form-control form-control-line">
                            </div>




                            <button type="submit"  class="btn btn-block waves-effect waves-light btn-primary">

                                Request Payment
                            </button>







                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- .row -->









        <a class="btn btn-block waves-effect waves-light btn-warning" href="{{route('admin.form.viewall.payment')}}">All Payments </a>
        <a class="btn btn-block waves-effect waves-light btn-success" href="{{route('admin.form.payment.get')}}">Create Payments </a>








       {{--main form end--}}

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer">
        © 2017 Northeastlive
    </footer>

</div>







@endsection