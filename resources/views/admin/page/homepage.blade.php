@extends('admin.index')

@section('content')


<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Starter kit</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">pages</li>
                <li class="breadcrumb-item active">Starter kit</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->





        {{--main form--}}




        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Request Payment</h4>
                        <h6 class="card-subtitle">Serach for user if exists</h6>

                        <form class="form-material m-t-40">


                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" name="fname"
                                       class="form-control form-control-line">
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="lname"
                                       class="form-control form-control-line">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email"
                                       class="form-control form-control-line">
                            </div>

                            <div class="form-group">
                                <label>Mobile</label>
                                <input type="text" name="mobile"
                                       class="form-control form-control-line">
                            </div>


                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" name="address"
                                       class="form-control form-control-line">
                            </div>


                            <div class="form-group">
                                <label>City</label>
                                <input type="text" name="city"
                                       class="form-control form-control-line">
                            </div>


                            <div class="form-group">
                                <label>State</label>
                                <input type="text" name="state"
                                       class="form-control form-control-line">
                            </div>




                            <div class="form-group">
                                <label>Pin</label>
                                <input type="text" name="pin"
                                       class="form-control form-control-line">
                            </div>


                            <div class="form-group">
                                <label>Amount</label>
                                <input type="number" name="amount"
                                       class="form-control form-control-line">
                            </div>




                            <button type="button" class="btn btn-block waves-effect waves-light btn-primary">

                                Request Payment
                            </button>







                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!-- .row -->

















        {{--main form end--}}

    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer">
        Northeastlive
    </footer>

</div>

    @endsection