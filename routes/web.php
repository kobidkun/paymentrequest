<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.pages.homepage');
});

Route::get('/terms', function () {
    return view('website.pages.terms');
});


Route::get('/refund', function () {
    return view('website.pages.refund');
});

Route::get('/privacy', function () {
    return view('website.pages.refund');
});

/*Route::get('/', function () {
    return view('admin.page.homepage');
});*/

//public controller

Route::get('/requested-payments/{slug}','publiccontroller\CustomerPublic@ShowPaymenttoUser');
Route::post('/requested-payments/submit','publiccontroller\CustomerPublic@ShowPaymenttoUsersubmit')->name('public.payment.form.submit');
//Route::get('/requested-payments/','publiccontroller\CustomerPaynow@ShowPaymenttoUser');
Route::get('/payment/status','publiccontroller\CustomerPublic@ReceievePaymentStatus');


//public controller

//Route::get('/', 'publiccontroller\CustomerPaynow@GetPaymentForm')->name('admin.form.payment.get');
Route::get('paynow', 'publiccontroller\CustomerPaynow@GetPaymentForm')->name('admin.form.payment.get');
Route::post('paynow', 'publiccontroller\CustomerPaynow@PostPaymentForm')->name('admin.form.payment.post');
Route::get('paynow/successful', 'publiccontroller\CustomerPaynow@GetPaymentFormsuccess')->name('admin.form.payment.success');
Route::get('paynow/status/{id}', 'publiccontroller\CustomerPaynow@viewpaymentdetails')->name('admin.form.payment.item.details');


Route::get('/payment/viewall','publiccontroller\CustomerPaynow@viewAllPayments')->name('admin.form.viewall.payment');
Route::get('/payment/viewall/api','publiccontroller\CustomerPaynow@viewAllPaymentsdatatableapi')
    ->name('admin.viewall.payment.api.datatable');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
