let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/


/*
mix.styles([
    'resources/assets/assets/plugins/bootstrap/css/bootstrap.min.css',
    'resources/assets/assets/plugins/morrisjs/morris.css',
    'resources/assets/css/style.css',
    'resources/assets/css/style.css',
    'resources/assets/css/colors/blue.css'
], 'public/css/admin.min.css');
*/




mix.styles([
    'resources/assets/website/css/base.css',
    'resources/assets/website/css/rs-plugin/css/settings.css',
    'resources/assets/website/css/extralayers.css',
], 'public/css/front/website.min.css').options({
    processCssUrls: true
});

/*mix.js([
   // 'resources/assets/assets/plugins/jquery/jquery.min.js',
    'resources/assets/assets/plugins/bootstrap/js/popper.min.js',
    'resources/assets/assets/plugins/bootstrap/js/bootstrap.min.js',
    'resources/assets/js/jquery.slimscroll.js',
    'resources/assets/js/waves.js',
    'resources/assets/js/sidebarmenu.js',
    'resources/assets/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
    'resources/assets/js/custom.min.js',
    'resources/assets/assets/plugins/sparkline/jquery.sparkline.min.js',
    'resources/assets/assets/plugins/raphael/raphael-min.js',
    'resources/assets/assets/plugins/morrisjs/morris.min.js',
    'resources/assets/js/dashboard1.js',

], 'public/js/admin.min.js')*/



mix.js([
   // 'resources/assets/assets/plugins/jquery/jquery.min.js',
    'resources/assets/website/js/jquery-2.2.4.min.js',
    'resources/assets/website/js/common_scripts_min.js',
    'resources/assets/website/js/functions.js',

], 'public/css/front/website.min.js')
